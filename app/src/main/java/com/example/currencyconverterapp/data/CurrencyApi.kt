package com.example.currencyconverterapp.data

import com.example.currencyconverterapp.data.models.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/v3/bc205609-f2be-4326-97db-fae689bbc02d")
    suspend fun getRates(
        @Query("base") base: String
    ): Response<CurrencyResponse>

}